package com.silvanacorrea.tarea01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesar.setOnClickListener {
            val edad: Int = edtEdad.text.toString().toInt()

            if(edad < 0){
                tvRespuesta.text = "No existen edades negativas"
            }else if(edad >= 18){
                tvRespuesta.text = "Usted es mayor de edad"
            }else{
                tvRespuesta.text = "Usted es menor de edad"
            }


        }
    }
}